{-# LANGUAGE OverloadedStrings #-}

module Client where

import           Control.Concurrent   (forkIO)
import           Control.Concurrent   (threadDelay)
import           Control.Monad        (forever)
import           Data.Aeson           (ToJSON, decode)
import           Data.Aeson.Text      (encodeToLazyText)
import qualified Data.ByteString.Lazy as LBS (hGet)
import           Data.Default.Class
import           Data.Maybe           (fromJust)
import           Data.Text            as T (Text, pack, unpack)
import           Network.HTTP.Req

import Network.WebSockets (ClientApp, Connection, receiveData, sendBinaryData,
                           sendClose, sendTextData)
import System.IO          (hFlush, hPrint, stderr, stdin, stdout)
import Wuss               (runSecureClient)

import Types

host :: String
host = "stream.watsonplatform.net"

uri :: String -> String
uri accessToken = "/speech-to-text/api/v1/recognize"
    <> "?access_token=" <> accessToken
    <> "&model=es-ES_BroadbandModel"

-- run :: FilePath -> IO ()
-- run apikeyFile = pack . filter (/= '\n') <$> readFile apikeyFile
--     >>= getAccessToken
--     >>= (\(AccessToken token) -> runSecureClient host 443 (uri token) app)

-- run :: FilePath -> IO ()
-- run token = filter (/= '\n') <$> readFile token
--     >>= (\accessToken -> runSecureClient host 443 (uri accessToken) app)

run :: String -> IO ()
run token = runSecureClient host 443 (uri token) app

sendFromStdin :: Connection -> Int -> IO ()
sendFromStdin conn bytes = do
    raw <- LBS.hGet stdin bytes
    sendBinaryData conn raw
    sendTextData conn $ encodeToLazyText stopRecognitionReq

app :: ClientApp ()
app conn = do
    putStrLn "-- Connected"

    -- Send audio data
    startRequest conn
    rawResponse <- receiveData conn
    _ <- case decode rawResponse :: Maybe StateResponse of
        Just (StateResponse "listening") -> do
            putStrLn "sending data"
            forkIO $ forever $ sendFromStdin conn 25600 -- 0.1 seconds at 256kbps (16bit, 16khz)
            pure ()
        Nothing -> print rawResponse

    -- Recive answers
    _ <- forever $ do
            rawResponse <- receiveData conn
            print rawResponse

            case decode rawResponse :: Maybe RecognitionResults of
                Just result -> do
                    putStr $ unpack $ prettyResult result
                    hFlush stdout
                Nothing -> pure ()

            case decode rawResponse :: Maybe ErrorResponse of
                -- Restart upon error
                Just (ErrorResponse e) -> startRequest conn >> hPrint stderr e
                Nothing -> pure ()

    -- FIXME: When to close the connection?
    sendClose conn ("" :: Text)

-- | Retrieve an 'AccessToken' given an /apikey/
getAccessToken :: Text -> IO AccessToken
getAccessToken apikey = runReq def $ do
    let params = ("grant_type" :: Text) =: grant <> "apikey" =: apikey
    bs <- req POST url (ReqBodyUrlEnc params) lbsResponse
    -- bs <- req POST url (NoReqBody) lbsResponse
        $  header "Content-Type" "application/x-www-form-urlencoded"
        <> header "Accept" "application/json"
    pure $ fromJust $ decode $ responseBody bs
    where
        url = https "iam.bluemix.net" /: "identity" /: "token"
        grant :: Text
        grant = "urn:ibm:params:oauth:grant-type:apikey"

startRequest :: Connection -> IO ()
startRequest conn = request startRecognitionReq conn >> threadDelay 100000

stopRequest :: Connection -> IO ()
stopRequest = request stopRecognitionReq

request :: ToJSON a => a -> Connection -> IO ()
request serviceReq conn = sendTextData conn $ encodeToLazyText serviceReq
