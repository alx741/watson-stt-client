module Main where

import Client
import Types

import Data.Text

main :: IO ()
main = do
    apikey <- pack . Prelude.filter (/= '\n') <$> readFile "api_key"
    (AccessToken token1) <- getAccessToken apikey
    token2 <- Prelude.filter (/= '\n') <$> readFile "access_token"
    -- print token1
    print token2
    run token2
    -- run "api_key"

-- arecord -f S16_LE -c1 -r 16000 -t raw | stack exec client
